import React from 'react';
import PropTypes from 'prop-types';

import ComponentInfo from 'component-info';
import Pagination from 'pagination';

import './Listing.scss';

import {serialize, deserialize} from 'libs/object';

/**
 * Listing
 * @description [Description]
 * @example
  <div id="Listing"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Listing, {
    	title : 'Example Listing'
    }), document.getElementById("Listing"));
  </script>
 */
class Listing extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'listing';

		this.state = {
			listing: this.getListing(props.items),
			filters: {
				sort: ''
			}
		};
	}

	updateFilters(obj = null) {
		// copy the current filters
		const filters = {...this.state.filters};

		// loop through all the filters, if there is a query that matches the filter name, add it
		for (let i in filters) {
			if (obj[i]) {
				filters[i] = obj[i];
			}
		}

		// update
		this.setState({filters});
	}

	componentDidMount() {
		// TODO: get the filters in the querystring, and populate the state filters with them
		window.addEventListener('popstate', e => {
			if (e.state && e.state.filters) {
				this.setState({filters: e.state.filters});
			}
		});

		if (!SERVER) {
			this.updateFilters(deserialize(window.location.search));
		}
	}

	onSort = e => {
		// TODO: work out if we need to make ajax requests, or we can perform the filtering below

		const value = e.target.value;
		const key = value.replace('-reverse', '');
		const isReverse = value.indexOf('-reverse') > -1;

		const sorted =
			this.props.items && key
				? this.props.items.sort((a, b) => {
						return a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0;
				  })
				: this.props.items;

		if (isReverse && key) {
			sorted.reverse();
		}

		this.setState(
			{
				listing: this.getListing(sorted),
				filters: {...this.state.filters, ...{sort: value}}
			},
			() => {
				this.setUrl();
			}
		);
	};

	setUrl() {
		if (window.history.pushState) {
			const newURL = new URL(window.location.href);

			newURL.search = '?' + serialize(this.state.filters);
			window.history.pushState({...{path: newURL.href}, ...{filters: {...this.state.filters}}}, '', newURL.href);
		}
	}

	getListing(items) {
		const {item} = this.props;

		const Component = item || ComponentInfo;

		return items
			.filter(n => n)
			.map((item, i) => {
				return (
					<div className={`${this.baseClass}__result`} key={`result-${i}`}>
						<Component {...item} />
					</div>
				);
			});
	}

	renderSort() {
		const {sort} = this.props;

		if (!sort) {
			return null;
		}

		const options = sort.map((option, i) => {
			return (
				<option key={`sort-${option.name}`} value={option.name}>
					{option.label}
				</option>
			);
		});

		return (
			<div>
				<label>Sort by:</label>
				<select onChange={this.onSort} value={this.state.filters.sort}>
					<option value="">Choose</option>
					{options}
				</select>
			</div>
		);
	}

	renderHeader() {
		return (
			<header>
				[header]
				<div>[showing 'x' of 'y' results]</div>
				<div>{this.renderSort()}</div>
			</header>
		);
	}

	renderFooter() {
		return <footer>[footer]</footer>;
	}

	renderFilters() {
		return <aside>[filters]</aside>;
	}

	renderResults() {
		const {listing} = this.state;

		// results based on an array of non-blank items
		const hasResults = listing ? listing.filter(n => n).length > 0 : false;

		return (
			<div className={`${this.baseClass}__results`}>
				{hasResults ? (
					<div>
						<div>{listing}</div>
						<Pagination />
					</div>
				) : (
					<div>no results</div>
				)}
			</div>
		);
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<ComponentInfo {...this.state.filters} />
				{this.renderHeader()}
				<div className={`${this.baseClass}__main`}>
					{this.renderFilters()}
					{this.renderResults()}
				</div>
				{this.renderFooter()}
			</div>
		);
	}
}

Listing.defaultProps = {
	item: null,
	items: []
};

Listing.propTypes = {
	item: PropTypes.any,
	items: PropTypes.array
};

export default Listing;
